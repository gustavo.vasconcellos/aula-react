import React from "react";

export default function Button(props) {
  const { content, updateValue } = props;

  return (
    <button
      className={`button-class-${content}`}
      onClick={() => updateValue(content)}
    >
      {content}
    </button>
  );
}
