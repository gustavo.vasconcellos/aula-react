import React, { Fragment } from "react";

// Stateless
export default function Funcao(props) {
  console.log(props.text);

  return (
    <div>
      <h1>{props.text}</h1>
    </div>
  );
}
