import React, { useState, useEffect, useReducer } from "react";
import "./App.css";
import Button from "./components/Button";

function App() {
  const [inputValue, setInputValue] = useState("");
  const [operation, setOperation] = useState("");

  const operationOptions = ["+", "-"];

  useEffect(() => {
    const haveOperation = operationOptions.some(options => {
      return (
        inputValue.indexOf(options) === inputValue.length - 1 ||
        inputValue.indexOf(options) !== -1
      );
    });

    if (!haveOperation) {
      setInputValue(`${inputValue} ${operation}`);
    }
  }, [operation]);

  const updateInputValue = number => {
    setInputValue(`${inputValue}${number}`);
  };

  return (
    <div className="App">
      <div className="input-container">
        <input value={inputValue} disabled type="text" />
      </div>
      <div className="board">
        {Array(10)
          .fill("")
          .map((_, index) => {
            return (
              <Button
                key={index}
                content={index}
                updateValue={updateInputValue}
              />
            );
          })}
      </div>
      <div className="operations-board">
        {operationOptions.map((operation, key) => {
          return (
            <Button
              key={operation}
              content={operation}
              updateValue={setOperation}
            />
          );
        })}
        <Button content="=" updateValue={setOperation} />
      </div>
    </div>
  );
}

export default App;
